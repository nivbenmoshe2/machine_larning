#install.packages('ggplot2')
#install.packages('dplyr')
library(ggplot2)
library(dplyr)

#data frame

df.raw <- read.csv(url('https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'))

str(df.raw)

df <- df.raw

df$Date <- as.Date(df$Date)

str(df)

df$Active <- df$Confirmed - df$Recovered - df$Deaths

df$Mort.rate <- df$Deaths/(df$Confirmed+1)

df.israel <- df %>% filter(Country == 'Israel')
#aes- �����
ggplot(df.israel, aes(Date, Confirmed)) + geom_point()
ggplot(df.israel, aes(Date, Active)) + geom_point()
ggplot(df.israel, aes(Date, Recovered)) + geom_point()
ggplot(df.israel, aes(Date, Deaths)) + geom_point()
ggplot(df.israel, aes(Date, Mort.rate)) + geom_point()

lag <- function(date, country, df, days){
  day <- date - days
  if(day >= df$Date[1]){
    v <- df %>% filter(Date == day, Country == country)
    return(v$Confirmed)
  }
  else{
    return(0)
  }
}

confirmed.lag <- mapply(lag, df$Date, df$Country, MoreArgs = list(df,5))

df$Confirmed.lag <- confirmed.lag
#df$confirmed.lag <- NULL #����� �����

df$Mort.rate.fixed <- df$Deaths/(df$Confirmed.lag+1)

df.israel <- df %>% filter(Country == 'Israel')

ggplot() + 
  geom_point(data = df.israel, aes(Date, Mort.rate.fixed), color = 'red') + 
  geom_point(data = df.israel, aes(Date, Mort.rate), color = 'blue')

df.sweden <- df %>% filter(Country == 'Sweden')

ggplot() + 
  #israel
  geom_point(data = df.israel , aes(Date, Confirmed), color = 'red') + 
  #sweden
  geom_point(data = df.sweden , aes(Date, Confirmed), color = 'blue')


ggplot() + 
  #israel
  geom_point(data = df.israel , aes(Date, Deaths), color = 'red') + 
  #sweden
  geom_point(data = df.sweden , aes(Date, Deaths), color = 'blue')


library(dplyr)
library(ggplot2)
setwd("C:/Users/nivbe/OneDrive/����� ������/�������/��� �/����� �/����� ��� ������ �����/datafilles")

df.raw <- read.csv(url('https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'))
str(df.raw)
df <- df.raw
df$Date <- as.Date(df$Date)
str(df)

df$Mort.rate <- df$Deaths/(df$Confirmed+1)


df.israel <- df %>% filter(Country == 'Israel')
df.italy <- df %>% filter(Country == 'Italy')
df.germany <- df %>% filter(Country == 'Germany')
df.united.kingdom <- df %>% filter(Country == 'United Kingdom')

#1
ggplot() + 
  #italy
  geom_point(data = df.italy , aes(Date, Confirmed), color = 'red') + 
  #germany
  geom_point(data = df.germany , aes(Date, Confirmed), color = 'blue')

#2
gt.ten <- function(x){
  if(x>=10) return(TRUE)
  return(FALSE)
}
filter.is <- sapply(df.israel$Confirmed, gt.ten)
filter.uk <- sapply(df.united.kingdom$Confirmed, gt.ten)

df.is.from10 <- df.israel[filter.is,]
df.uk.from10 <- df.united.kingdom[filter.uk,]

days.is <- 1:nrow(df.is.from10)
days.uk <- 1:nrow(df.uk.from10)

df.is.from10$Days <- days.is
df.uk.from10$Days <- days.uk



ggplot() + 
  #israel
  geom_point(data = df.is.from10 , aes(Days, Confirmed), color = 'red') + 
  #united kingdom
  geom_point(data = df.uk.from10 , aes(Days, Confirmed), color = 'blue')

#3
lag <- function(date, country, df, days){
  day <- date - days
  if(day >= df$Date[1]){
    v <- df %>% filter(Date == day, Country == country)
    return(v$Confirmed)
  }
  else{
    return(0)
  }
}

confirmed.lag <- mapply(lag, df$Date, df$Country, MoreArgs = list(df,10))

df$Confirmed.lag <- confirmed.lag

df$Mort.rate.fixed <- df$Deaths/(df$Confirmed.lag+1)

df.italy <- df %>% filter(Country == 'Italy')
df.germany <- df %>% filter(Country == 'Germany')
df.united.kingdom <- df %>% filter(Country == 'United Kingdom')


ggplot() + 
  #italy
  geom_point(data = df.italy , aes(Date, Mort.rate.fixed), color = 'red') + 
  #germany
  geom_point(data = df.germany , aes(Date, Mort.rate.fixed), color = 'blue') +
  #united kingdom
  geom_point(data = df.united.kingdom , aes(Date, Mort.rate.fixed), color = 'green') +
  ylim(0,1)

#############


#build column chart for mort rate

contries <- df %>% distinct(Country)

interesting.countries <- c('Belgium', 'Denmark', 'France', 'Germany', 'Greece', 'Israel', 'Italy', 'Japan', 'Sweden', 'Switzerland', 'United Kingdom', 'US')

df.int.countries <- df %>% filter(Country %in% interesting.countries)

today <- as.Date('2020-06-25')

df.int.countries.today <-  df.int.countries %>% filter(Date == today)

ggplot(df.int.countries.today, aes(reorder(Country,-Mort.rate.fixed),Mort.rate.fixed)) + geom_col()

#����� ����� �����
write.csv(df.int.countries.today, file = 'corona-01-05.csv')

#����� �����
countries.age <- read.csv('corona-01-05.csv')

str(countries.age)


ggplot(countries.age, aes(age, Mort.rate.fixed)) + geom_point() + 
  geom_text(aes(label = Country), hjust = 0, vjust = 0)


df.aggragate <- df %>% group_by(Date) %>% summarize(total = sum(Confirmed))

day.diff <- function(date,df){
  today.df <- df %>% filter(Date == date)
  today <- today.df$total
  yesterday.df <- df %>% filter (Date == date - 1)
  if(nrow(yesterday.df) == 0) return(0)
  yesterday <- yesterday.df$total
  return(today - yesterday)
}

diff <- sapply(df.aggragate$Date, day.diff, df = df.aggragate)

df.aggragate$diff <- diff

ggplot(df.aggragate, aes(Date,diff)) + geom_area()


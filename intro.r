x <- 5
y <- 7
x
y
x[1] <- 7
x[2] <- 8
#x[3] <- 'car'

#expressions (vector operations)
y <- x*2
Z <- x**2

#test data type
class(x)

#data type conversion
x <- as.character(x)
x <- as.integer(x)

#create vector with combine
x <- c(12 , 4 , 8)
#edd ������
x <- c(x , 19 , 20)

#create vector with sequence
y <- 1:6
z <- 35:789

#subseting vectors
v <- c(45 , 56 , 87 , 94 , 56 , 56 , 77)
w <- v[3:6]
z <- v[5]
z <- v[c(1,6,7)]
z <- v[v>60] #filter
t <- v>60

#naming vector items
salary <- c(8000 , 6000 , 4000 , 4000)
names(salary) <- c('jack','john','john','alice')
salary[1]
salary['jack']
salary['john']

#A general method to create a sequence
s <- seq(4,9,length = 7)

#random smapling
smp <- sample(v, 10 , replace = T)
smp1 <- sample(v, 5)

smp2 <- sample(v, 7)
?sample

#comuting mean
meanv <- mean(v)

v.with.na <- c(v,NA)

mean(v.with.na) #error
mean(v.with.na, na.rm = T) #mean without na

#handling NAs
is.na(v.with.na)
filter.na <- is.na(v.with.na)
v.with.na[filter.na]
v.with.na[!filter.na]

#functions

tofar <- function(cel = 5){
  far <- cel*1.8 + 32
  return(far)
}

tofar(30)
tofar()




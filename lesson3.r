##EX1
#1
#s- order cost , d- demand in units , Q- order size , h- holding cost
eoq <- function(d,s,h){
  return(sqrt((2*d*s)/h))
}
#2
 h <- seq(100,1000,length.out = 20)
 eoqs <- eoq(1000,1500,h)
 plot(eoqs)  #���
 
 #3
 filter <- eoqs>100
eoqs.more.than.100 <- eoqs[filter] 
plot(eoqs.more.than.100) 


########################################################################################
  

#sampling from distributions
x <- rnorm(10,11,2)
y <- runif(10,2,9)


#sapply- ����� ������� �� �����
v <- c(1,11,13,4,5)

cut5 <- function(x){
  if(x<5){
    return(x)
  }
  else {
    return(5)
  }
}

vmax5 <- sapply(v,cut5)

#Monte carlo simulasion

project.length <- function(x){
  a.time <- rnorm(1,10,2)
  b.time <- runif(1,5,13)
  c.time <- rnorm(1,16,3)
  if(a.time + b.time > c.time){
    project.time = a.time + b.time
  }
  else{
    project.time = c.time
  }
  return(project.time)
}
project.length()

samples <- 1:10000

proc.time.vec <- sapply(samples,project.length)

hist(proc.time.vec,100) #��������� �� �����

#matrices

v1 <- c(1,2,3,4,5)
v2 <- c(5,6,7,8,9)

mat1 <- cbind(v1,v2)
mat2 <- rbind(v1,v2)

class(mat1)
typeof(mat1)

t(mat1)


#apply function

mat <- matrix(rnorm(24,5,2),8,3)

columns.means <- apply(mat,2,mean) # 1-rows , 2 - columns

rows.means <- apply(mat,1,mean)

cut5.all <- apply(mat,c(1,2),cut5)






